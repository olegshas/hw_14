//
//  ErrorDomain.swift
//  HW13
//
//  Created by Oleg Shashkov on 12/08/2019.
//  Copyright © 2019 citymed12. All rights reserved.
//

import Foundation
public let OSNetworkingErrorDomain = "ru.citymed12.CityMed.NetworkingError"
public let MissingHTTPResponseError = 100
public let UnexpectedResponseError = 200
